<?php

return [
    'admin' => [
        'default_email' => env('ADMIN_DEFAULT_EMAIL', 'dev@tallium.com'),
        'default_password' => env('ADMIN_DEFAULT_PASSWORD', 'admin'),
    ],
    'checks_enabled' => env('OFFICE_LIGHT_CHECKS_ENABLED', true),
    'ip_address' => '62.216.37.46',
    'light_checks_expiration_time_in_days' => 7,
    'login_token_expiration_time_in_minutes' => 15,
    'light_checks_interval_in_minutes' => 5,

    'telegram' => [
        'debug_mode' => env('TELEGRAM_DEBUG_MODE', false),
        'bot_url' => 'https://t.me/office_checker_bot',
        'webhook' => [
            'handler' => [
                'namespace' => 'App\\Services\\Telegram\\WebhookHandlers',
                'path' => app_path('Services/Telegram/WebhookHandlers'),
            ],
        ],
    ],

    'test_mode' => [
        'enabled' => env('TEST_MODE_ENABLED', false),
        'active_telegram_username' => env('TEST_MODE_ACTIVE_TELEGRAM_USERNAME', 'bokoch'),
    ],
];