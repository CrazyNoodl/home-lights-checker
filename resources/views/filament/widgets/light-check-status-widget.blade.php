<x-filament::widget class="filament-light-check-status-widget">
    <x-filament::card>

        <div class="relative p-2 rounded-2xl bg-white dark:bg-gray-800">
            <div class="flex items-center space-x-2 rtl:space-x-reverse text-sm font-medium text-gray-500 dark:text-gray-200">
                <span>Current office light status</span>
            </div>
            <div @class([
                'text-3xl',
                'text-success-600' => $lightAvailable,
                'text-danger-600' => ! $lightAvailable,
                'font-medium',
                'flex',
                'items-center',
                'space-x-2',
                'py-2',
            ])>
                <x-dynamic-component
                        :component="$lightAvailable ? 'heroicon-o-check-circle' : 'heroicon-o-x-circle'"
                        :class="'w-7 h-7'"
                />
                <span>
                    {{ $lightAvailable ? 'Enabled' : 'Disabled' }}
                </span>
            </div>
            <div class="text-sm">
                <div class="text-primary-600 font-medium">Since: {{ $fromDate }}</div>
                <div class="text-gray-400 italic">Last check: {{ $lastCheckDate }}</div>
            </div>
        </div>
    </x-filament::card>
</x-filament::widget>
