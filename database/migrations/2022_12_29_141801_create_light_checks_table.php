<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::create('light_checks', function (Blueprint $table) {
            $table->id();

            $table->string('ip_address');
            $table->boolean('light_available')->default(false);
            $table->text('output_description')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('light_checks');
    }
};