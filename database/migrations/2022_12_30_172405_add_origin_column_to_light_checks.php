<?php

use App\Enums\LightCheckOrigin;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::table('light_checks', function (Blueprint $table) {
            $table->enum('origin', LightCheckOrigin::values())->after('output_description')->default(LightCheckOrigin::CRON->value);
        });
    }

    public function down()
    {
        Schema::table('light_checks', function (Blueprint $table) {
            $table->dropColumn('origin');
        });
    }
};
