<?php

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class TestModeScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $builder->where('name', config('office_light.test_mode.active_telegram_username'));
    }
}