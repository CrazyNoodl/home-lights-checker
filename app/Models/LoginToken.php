<?php

namespace App\Models;

use App\EloquentBuilders\LoginTokenBuilder;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Prunable;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 * @property int $user_id
 * @property string $token
 * @property Carbon $expires_at
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property-read User $user
 *
 * @method static LoginTokenBuilder query()
 */
class LoginToken extends Model
{
    use Prunable;

    protected $fillable = [
        'user_id',
        'token',
        'expires_at',
    ];

    protected $dates = [
        'expires_at',
    ];

    public function newEloquentBuilder($query): LoginTokenBuilder
    {
        return new LoginTokenBuilder($query);
    }

    public function prunable()
    {
        return static::query()->expired();
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
