<?php

namespace App\Models;

use App\EloquentBuilders\LightCheckBuilder;
use App\Enums\LightCheckOrigin;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Prunable;

/**
 * @property int $id
 * @property string $ip_address
 * @property bool $light_available
 * @property string|null $output_description
 * @property LightCheckOrigin $origin
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @method static LightCheckBuilder query()
 */
class LightCheck extends Model
{
    use Prunable;

    protected $fillable = [
        'ip_address',
        'light_available',
        'output_description',
        'origin',
    ];

    protected $casts = [
        'light_available' => 'bool',
        'origin' => LightCheckOrigin::class,
    ];

    public function newEloquentBuilder($query): LightCheckBuilder
    {
        return new LightCheckBuilder($query);
    }

    public function prunable(): Builder
    {
        $expiredDate = now()->subDays(
            config('office_light.light_checks_expiration_time_in_days')
        );

        return static::query()
            ->where('created_at', '<', $expiredDate);
    }
}
