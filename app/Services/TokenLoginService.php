<?php

namespace App\Services;

use App\Models\LoginToken;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

readonly class TokenLoginService
{
    public function __construct(
        private string $loginTokenExpirationTimeInMinutes
    ) {
    }

    public function generateLoginLink(User $user): string
    {
        $token = $this->createLoginToken($user);

        return route('login.token', $token);
    }

    public function resolveLoginToken(string $accessToken): LoginToken|null
    {
        $accessTokenParts = explode('|', $accessToken);
        $loginTokenId = $accessTokenParts[0] ?? null;
        $token = $accessTokenParts[1] ?? '';

        /** @var LoginToken|null $loginToken */
        $loginToken = LoginToken::query()
            ->where('id', $loginTokenId)
            ->notExpired()
            ->first();

        if ($loginToken && Hash::check($token, $loginToken->token)) {
            return $loginToken;
        }

        return null;
    }

    private function createLoginToken(User $user): string
    {
        $token = Str::random(60);
        $expiresAt = now()->addMinutes($this->loginTokenExpirationTimeInMinutes);

        /** @var LoginToken $loginToken */
        $loginToken = $user->loginTokens()->create([
            'token' => Hash::make($token),
            'expires_at' => $expiresAt,
        ]);

        return $loginToken->id . '|' . $token;
    }
}
