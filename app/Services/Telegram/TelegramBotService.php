<?php

namespace App\Services\Telegram;

use App\DTO\TelegramMessage;
use App\Models\TelegramChat;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

readonly class TelegramBotService
{
    public function __construct(
        private string $baseUri,
        private string $botApiToken,
    ) {
    }

    /**
     * @throws RequestException
     */
    public function sendMessage(TelegramMessage $message): void
    {
        try {
            $this->sendRequest('sendMessage', $message->toArray());
        } catch (RequestException $e) {
            if ($e->getCode() === 403) {
                TelegramChat::query()->where('telegram_chat_id', $message->chatId)->delete();
            }

            throw $e;
        }
    }

    /**
     * @throws RequestException
     */
    public function getUpdates(): array
    {
        return $this->sendRequest('getUpdates')->json()['result'] ?? [];
    }

    /**
     * @throws RequestException
     */
    private function sendRequest(string $action, array $payload = []): Response
    {
        $url = sprintf('/bot%s/%s', $this->botApiToken, $action);

        return Http::baseUrl($this->baseUri)
            ->asJson()
            ->post($url, $payload)
            ->throw();
    }
}
