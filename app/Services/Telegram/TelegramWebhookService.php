<?php

namespace App\Services\Telegram;

use App\DTO\TelegramWebhookData;
use App\Models\TelegramChat;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

readonly class TelegramWebhookService
{
    public function __construct(private TelegramWebhookHandlerResolverService $telegramWebhookHandlerResolverService)
    {
    }

    public function handleWebhook(array $telegramWebhookData): void
    {
        if (config('office_light.telegram.debug_mode')) {
            Log::debug($telegramWebhookData);
        }

        $validator = Validator::make($telegramWebhookData, [
            'message.text' => ['required'],
            'message.chat.id' => ['required'],
            'message.chat.username' => ['sometimes'],
        ]);

        if ($validator->fails()) {
            return;
        }

        $telegramWebhookData = TelegramWebhookData::from($telegramWebhookData);

        TelegramChat::updateOrCreateFromWebhookData($telegramWebhookData);

        $webhookHandler = $this->telegramWebhookHandlerResolverService->resolveWebhookHandler($telegramWebhookData);

        if (! $webhookHandler) {
            return;
        }

        $webhookHandler->handle($telegramWebhookData);
    }
}
