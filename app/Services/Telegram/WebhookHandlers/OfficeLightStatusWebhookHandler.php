<?php

namespace App\Services\Telegram\WebhookHandlers;

use App\Attributes\TelegramWebhookAction;
use App\DTO\TelegramWebhookData;
use App\Jobs\PerformLightCheckForUserJob;

#[TelegramWebhookAction(['/office_light_status', '/start'])]
readonly class OfficeLightStatusWebhookHandler implements WebhookHandler
{
    public function handle(TelegramWebhookData $telegramWebhookData): void
    {
        PerformLightCheckForUserJob::dispatch($telegramWebhookData->chatId);
    }
}
