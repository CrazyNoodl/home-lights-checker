<?php

namespace App\Filament\Resources\LightCheckResource\Pages;

use App\Filament\Resources\LightCheckResource;
use Filament\Resources\Pages\ManageRecords;

class ManageLightChecks extends ManageRecords
{
    protected static string $resource = LightCheckResource::class;

    protected function getHeaderWidgets(): array
    {
        return [
            LightCheckResource\Widgets\LightCheckDataRetentionInfo::class,
            LightCheckResource\Widgets\LightCheckIntervalsInfo::class,
        ];
    }

    protected function getActions(): array
    {
        return [
            //
        ];
    }
}
