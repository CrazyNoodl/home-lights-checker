<?php

namespace App\Filament\Resources;

use App\Filament\Resources\LightCheckIntervalResource\Pages;
use App\Filament\Resources\LightCheckResource\Widgets\LightCheckDataRetentionInfo;
use App\Filament\Resources\LightCheckResource\Widgets\LightCheckIntervalsInfo;
use App\Models\LightCheck;
use Filament\Forms\Components\DateTimePicker;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables\Columns\IconColumn;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Filters\Filter;
use Filament\Tables\Filters\TernaryFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class LightCheckIntervalResource extends Resource
{
    protected static ?string $model = LightCheck::class;

    protected static ?string $modelLabel = 'Light Check Interval';

    protected static ?string $slug = 'light-check-intervals';

    protected static ?string $navigationIcon = 'heroicon-o-calendar';

    protected static ?int $navigationSort = 2;

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                //
            ]);
    }

    public static function getEloquentQuery(): Builder
    {
        return LightCheck::query()
            ->select([DB::raw('row_number() over (order by from_date) as id'), '*'])
            ->fromSub(
                LightCheck::query()
                    ->selectRaw('
                        min(created_at_prev) as from_date,
                        max(created_at) as to_date,
                        light_available
                    ')
                    ->fromSub(
                        DB::table('light_checks')
                            ->selectRaw('
                                  created_at,
                                  light_available,
                                  row_number() over (order by created_at) as rn_all,
                                  row_number() over (PARTITION BY light_available ORDER BY created_at) as rn_group,
                                  lag(created_at, 1, created_at) over (order by created_at) as created_at_prev
                            '),
                        'lc'
                    )
                    ->groupByRaw('rn_all - rn_group, light_available')
                    ->orderByDesc('from_date'),
                'lc'
            );
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('from_date')->dateTime(),
                TextColumn::make('to_date')->dateTime(),
                IconColumn::make('light_available')->boolean(),
            ])
            ->filters([
                TernaryFilter::make('light_available'),
                Filter::make('date')
                    ->form([
                        DateTimePicker::make('from_date'),
                        DateTimePicker::make('to_date'),
                    ])
                    ->query(function (Builder $query, array $data): Builder {
                        return $query
                            ->when(
                                $data['from_date'],
                                fn(Builder $query, $date): Builder => $query->where('from_date', '>=', $date),
                            )
                            ->when(
                                $data['to_date'],
                                fn(Builder $query, $date): Builder => $query->where('to_date', '<=', $date),
                            );
                    })
            ])
            ->defaultSort('from_date', 'desc')
            ->bulkActions([
                //
            ])
            ->actions([
                //
            ]);
    }

    public static function getWidgets(): array
    {
        return [
            LightCheckDataRetentionInfo::class,
            LightCheckIntervalsInfo::class,
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ManageLightCheckIntervals::route('/'),
        ];
    }

    public static function getGloballySearchableAttributes(): array
    {
        return [];
    }
}