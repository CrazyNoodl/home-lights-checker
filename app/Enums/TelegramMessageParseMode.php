<?php

namespace App\Enums;

enum TelegramMessageParseMode: string
{
    case MARKDOWN = 'Markdown';

    case MARKDOWN_V2 = 'MarkdownV2';

    case HTML = 'HTML';
}