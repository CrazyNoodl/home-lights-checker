<?php

namespace App\Console\Commands;

use App\Services\OfficeLightCheckerService;
use Illuminate\Console\Command;

class CheckOfficeLightCommand extends Command
{
    protected $signature = 'check:office-light';

    protected $description = 'Run checking of office light';

    public function handle(OfficeLightCheckerService $officeLightCheckerService): int
    {
        $this->components->info('Running office light check...');

        $officeLightCheckerService->runCheck();

        $this->components->info('Office light check was ran successfully.');

        return self::SUCCESS;
    }
}
