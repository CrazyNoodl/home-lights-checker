<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule): void
    {
        $officeLightChecksIntervalInMinutes = config('office_light.light_checks_interval_in_minutes');

         $schedule->command('check:office-light')
             ->when(
                 fn() => config('office_light.checks_enabled')
             )
             ->cron(
                 sprintf('*/%s * * * *', $officeLightChecksIntervalInMinutes)
             );

         $schedule->command('model:prune')->daily();

         $schedule->command('backup:run --only-db --disable-notifications')->daily();
         $schedule->command('backup:clean --disable-notifications')->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands(): void
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
