<?php

namespace App\EloquentBuilders;

use App\Enums\LightCheckOrigin;
use Illuminate\Database\Eloquent\Builder;

class LightCheckBuilder extends Builder
{
    public function origin(LightCheckOrigin $origin = LightCheckOrigin::CRON): static
    {
        return $this->where('origin', $origin);
    }
}
