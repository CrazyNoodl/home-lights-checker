<?php

namespace App\Listeners;

use App\DTO\LightCheckStatusData;
use App\Events\LightCheckPerformedEvent;
use App\Jobs\SendMessageToTelegramChatJob;
use App\Models\LightCheck;
use App\Models\TelegramChat;
use App\DTO\TelegramMessage;

readonly class SendLightCheckStatusNotificationListener
{
    public function handle(LightCheckPerformedEvent $event): void
    {
        $lightCheck = $event->lightCheck;

        /** @var LightCheck|null $lastLightCheck */
        $lastLightCheck = LightCheck::query()
            ->where('ip_address', $lightCheck->ip_address)
            ->where('id', '<>', $lightCheck->id)
            ->origin()
            ->latest()
            ->first();

        if ($lastLightCheck?->light_available === $lightCheck->light_available) {
            return;
        }

        $lightCheckStatusData = LightCheckStatusData::from($lightCheck);

        TelegramChat::query()
            ->pluck('telegram_chat_id')
            ->each(fn (string $chatId) => SendMessageToTelegramChatJob::dispatch(
                TelegramMessage::make($lightCheckStatusData->getStatusMessage(), $chatId)
            ));
    }
}