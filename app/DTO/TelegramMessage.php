<?php

namespace App\DTO;

use App\Enums\TelegramMessageParseMode;

class TelegramMessage
{
    /** @var array<TelegramInlineKeyboardButton> */
    public array $telegramInlineKeyboardButtons = [];

    public function __construct(
        public readonly string $content,
        public readonly string $chatId,
        public readonly TelegramMessageParseMode $parseMode = TelegramMessageParseMode::MARKDOWN
    ) {
    }

    public static function make(string $content, string $chatId): static
    {
        return new static($content, $chatId);
    }

    public function urlButton(TelegramInlineKeyboardButton $button): static
    {
        $this->telegramInlineKeyboardButtons[] = $button;

        return $this;
    }

    public function toArray(): array
    {
        $payload = [
            'chat_id' => $this->chatId,
            'text' => $this->content,
            'parse_mode' => $this->parseMode->value,
        ];

        if (! empty($this->telegramInlineKeyboardButtons)) {
            $payload['reply_markup']['inline_keyboard'] = [
                array_map(
                    fn(TelegramInlineKeyboardButton $button) => $button->toArray(),
                    $this->telegramInlineKeyboardButtons
                )
            ];
        }

        return $payload;
    }
}
