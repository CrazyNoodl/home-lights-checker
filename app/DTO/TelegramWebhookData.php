<?php

namespace App\DTO;

readonly class TelegramWebhookData
{
    public function __construct(
        public string $chatId,
        public string $messageText,
        public string $username,
    ) {
    }

    public static function from(array $telegramWebhookData): static
    {
        return new static(
                $telegramWebhookData['message']['chat']['id'],
            $telegramWebhookData['message']['text'],
            $telegramWebhookData['message']['chat']['username'] ?? 'n/a'
        );
    }
}
