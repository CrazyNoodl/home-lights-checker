<?php

namespace App\Jobs;

use App\Services\Telegram\TelegramBotService;
use App\DTO\TelegramMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Client\RequestException;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SendMessageToTelegramChatJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public function __construct(
        private readonly TelegramMessage $telegramMessage
    ) {
    }

    public function handle(TelegramBotService $telegramBotService): void
    {
        try {
            $telegramBotService->sendMessage($this->telegramMessage);
        } catch (RequestException $e) {
            Log::error('Cannot send message to telegram chat. Failed with error: ' . $e->getMessage(), [
                $this->telegramMessage->toArray()
            ]);
        }
    }
}